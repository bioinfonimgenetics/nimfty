# -*- coding: utf-8 -*-

import logging

from handlers.alerts_handler import AlertsHandler
from core.nimfty_core import NimftyCore

_logger = logging.getLogger('nimfty')


def start_nimfty():
    """
    Starts the NIMFty process

    :return: None
    """
    _logger.info('Main NIMFty process has started')

    # Alerts handler (Borg pattern, see class doc)
    AlertsHandler()

    # No arguments needed,so run NIMFty Core
    nimfty_core = NimftyCore()
    nimfty_core.run_nimfty()

    _logger.info('Main NIMFty process has finished')

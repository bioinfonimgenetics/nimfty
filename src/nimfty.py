# -*- coding: utf-8 -*-

import nimfty_starter as starter

from config.config_parser import ConfigParser
from config.logging_parser import LoggingParser
from nimutils import (numbers as nim_numbers, dashboard as nim_dashboard)


# noinspection SpellCheckingInspection
if __name__ == "__main__":
    """
    NIMFty is an automated process that allows the massive insertion of data in GestLab from BGI NIfty
    Main class and entry point of the execution
    """
    # Id execution
    _id_execution = nim_numbers.random_number()
    nim_dashboard.program_status(str(_id_execution), type_msg='id')

    # Configuration initialization
    ConfigParser.initialize_config()

    # Logging initialization
    LoggingParser.initialize_logging(_id_execution)

    # Entry point of NIMFty process
    starter.start_nimfty()

    # End of emedNIM
    nim_dashboard.program_status('NIMFty has finished', type_msg='end')

# -*- coding: utf-8 -*-

import os
import logging
import pandas as pd

import errors.errors as e

from statics import queries as qs
from statics.constants import generic as cts
from config.config_parser import ConfigParser
from handlers.alerts_handler import AlertsHandler
from structs.structures_controller import StructuresController
from nimutils import (dashboard as nim_dashboard, db as nim_db, files as nim_files)


class NimftyHandler:
    """
    Defines all the operations that are required to process all the info that will be
    inserted into GestLab DB in a massive way
    """
    def __init__(self):
        """ Constructor """
        self._nimfty_handler_logger = logging.getLogger('nimfty_handler')
        self._nimfty_df = pd.DataFrame()


    def read_bgi_files(self):
        """
        Reads BGI NIFTY files from folder by creating a global dataframe with all its info

        :return: None
        """
        msg = 'Reading BGI NIFTY files'
        self._nimfty_handler_logger.info(msg)
        nim_dashboard.program_status(msg, type_msg='info')

        bgi_folder = ConfigParser.get_conf('PATHS', 'bgi_folder')
        for root, dirs, files in os.walk(bgi_folder):
            for filename in files:
                # Temporal file structure...
                file_df = pd.read_excel(os.path.join(root, filename), usecols=cts.DF_COLS).dropna(axis=0, how='all')
                # ... to be appended to main structure
                self._nimfty_df = self._nimfty_df.append(file_df, ignore_index=True, sort=False)
                # Add alert of file processed
                self._nimfty_handler_logger.info(f'BGI file processed: {filename}')
                AlertsHandler.alerts_files_read.append(filename)


    def bgi_files(self):
        """
        Returns a flag indicating if there are BGI files to process or not

        :return: the flag
        """
        bgi_files = not self._nimfty_df.empty
        if not bgi_files:
            msg = f'No files in BGI folder to process. Please check this'
            self._nimfty_handler_logger.warning(msg)
            e.raise_nimfty_warning(msg)
        return bgi_files


    def format_bgi_info(self):
        """
        Formats the BGI Nifty dataframe info

        :return: None
        """
        msg = 'Formatting BGI NIFTY info'
        self._nimfty_handler_logger.info(msg)
        nim_dashboard.program_status(msg, type_msg='info')

        # Rename Patient_Name & Unnamed:26 to Sample_ID for better readability
        self._nimfty_df.rename(columns={cts.PATIENT_NAME: cts.SAMPLE_ID}, inplace=True)
        # Format cffDNA percentage
        cffdna_fomatted = self._nimfty_df[cts.CFFDNA].apply(lambda x: x.rstrip('%') if not pd.isnull(x) else x)
        self._nimfty_df[cts.CFFDNA] = cffdna_fomatted
        # Retrieve each sample type
        self._nimfty_df.insert(0, cts.SAMPLE_TYPE, self._nimfty_df[cts.SAMPLE_ID].apply(self._query_sample_type))


    def prepare_gestlab_structures(self):
        """
        Prepares Gestlab structures by creating a csv file for each different sample type

        :return: None
        """
        msg = 'Preparing Gestlab structures for each sample'
        self._nimfty_handler_logger.info(msg)
        nim_dashboard.program_status(msg, type_msg='info')

        # Group the NIMFty dataframe by sample type
        trisonim_groups = self._nimfty_df.groupby(cts.SAMPLE_TYPE)

        structures_controller = StructuresController()

        # For each group of samples by type, process the info
        for sample_type, trisonim_group in trisonim_groups:
            # Process each group type
            trisonim_results = structures_controller.dispatch_group_by_sample_type(trisonim_group, sample_type)
            # Write the group results
            structures_controller.write_group_by_sample_type(trisonim_results, sample_type)


    def clear_bgi_folder(self):
        """
        Removes all the content in BGI folder

        :return: None
        """
        msg = 'Removing processed files from BGI folder'
        self._nimfty_handler_logger.info(msg)
        nim_dashboard.program_status(msg, type_msg='info')

        nim_files.clear(ConfigParser.get_conf('PATHS', 'bgi_folder'))


    def _query_sample_type(self, petition):
        """
        Given a petition ID (for example 18NT5952), executes a query to retrieve its sample type id

        For example, 18NT5952 includes 'TrisoNIM Premium' and it has the sample type ID = 2402

        :return: None
        """
        query = qs.GESTLAB_SAMPLE_TYPE_ID_BY_PETITION_ID.format(petition)
        query_df = nim_db.execute_query_fdb(query)

        sample_type = None

        if query_df.empty:
            msg = f'Petition ID {petition} is not a NIFTY type and will be excluded'
            e.raise_nimfty_warning(msg)
            self._nimfty_handler_logger.warning(msg)
            AlertsHandler.alerts_samples_excluded.append(petition)
        else:
            sample_type = query_df[cts.SAMPLE_TYPE.upper()].iloc[0]
            AlertsHandler.alerts_samples_included[cts.BGI_NIFTY_TESTS_NAMES[sample_type]].append(petition)

        return sample_type

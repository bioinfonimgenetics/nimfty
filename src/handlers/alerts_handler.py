# -*- coding: utf-8 -*-

from collections import defaultdict

from statics import email as cts_email
from nimutils import others as nim_others
from config.config_parser import ConfigParser
from nimutils import dashboard as nim_dashboard


class AlertsHandler:
    """
    Defines all the operations that are required to process all the NIMFty alerts.
    This handler uses class variables that have the same value across all class instances
    """
    _to_send = False
    alerts_files_read = []
    alerts_samples_included = defaultdict(list)
    alerts_samples_excluded = []
    alerts_samples_high_risk = defaultdict(list)
    alerts_samples_requests = defaultdict(list)
    alerts_samples_info = defaultdict(list)

    @classmethod
    def send_alerts(cls):
        """
        Sends alerts (emails) about the process execution

        :return: None
        """
        # Email composition
        email_to = ConfigParser.get_conf('ALERTS', 'email_to').split(',')
        email_cc = ConfigParser.get_conf('ALERTS', 'email_cc').split(',')
        email_subject = ConfigParser.get_conf('ALERTS', 'email_subject')
        email_body = cts_email.NIMFTY_EMAIL_HEAD            # Add head section to body
        email_body += cls._build_alerts_info_section()      # Add info section to body
        email_body += cls._build_alerts_results_section()   # Add results section to body
        email_body += cts_email.NIMFTY_EMAIL_END

        if cls._to_send:
            msg = f'Sending the Alerts mail'
            nim_dashboard.program_status(msg, type_msg='info')
            nim_others.send_email(email_body, to=email_to, cc=email_cc, subject=email_subject)


    @classmethod
    def _build_alerts_info_section(cls):
        """
        Builds the info section for alerts email

        :return: the email body with the info section
        """
        section = ''
        if cls.alerts_files_read or cls.alerts_samples_included or cls.alerts_samples_excluded:
            cls._to_send = True
            section += cts_email.NIMFTY_EMAIL_INFO_SECTION
            if cls.alerts_files_read:
                section += cts_email.NIMFTY_EMAIL_ROW.format(cts_email.NIMFTY_EMAIL_TITLE_FILES_READ,
                                                             cls.__build_simple_alert(cls.alerts_files_read))
            if cls.alerts_samples_included:
                section += cts_email.NIMFTY_EMAIL_ROW.format(cts_email.NIMFTY_EMAIL_TITLE_SAMPLES_INCLUDED,
                                                             cls.__build_complex_alert(cls.alerts_samples_included))
            if cls.alerts_samples_excluded:
                section += cts_email.NIMFTY_EMAIL_ROW.format(cts_email.NIMFTY_EMAIL_TITLE_SAMPLES_EXCLUDED,
                                                             cls.__build_simple_alert(cls.alerts_samples_excluded))
        return section


    @classmethod
    def _build_alerts_results_section(cls):
        """
        Builds the results section for alerts email

        :return: the email body with the info section
        """
        section = ''
        if cls.alerts_samples_high_risk or cls.alerts_samples_requests or cls.alerts_samples_info:
            cls._to_send = True
            section += cts_email.NIMFTY_EMAIL_RESULTS_SECTION
            if cls.alerts_samples_high_risk:
                section += cts_email.NIMFTY_EMAIL_ROW.format(cts_email.NIMFTY_EMAIL_TITLE_SAMPLES_HIGH_RISK,
                                                             cls.__build_complex_alert(cls.alerts_samples_high_risk))
            if cls.alerts_samples_requests:
                section += cts_email.NIMFTY_EMAIL_ROW.format(cts_email.NIMFTY_EMAIL_TITLE_SAMPLES_REQUESTS,
                                                             cls.__build_complex_alert(cls.alerts_samples_requests))
            if cls.alerts_samples_info:
                section += cts_email.NIMFTY_EMAIL_ROW.format(cts_email.NIMFTY_EMAIL_TITLE_SAMPLES_INFO,
                                                             cls.__build_complex_alert(cls.alerts_samples_info))
        return section


    @classmethod
    def __build_simple_alert(cls, simple_list):
        """
        Build a simple alert for a given list

        :param simple_list: the list to build an alert for

        :return: the alert for the simple list
        """
        return cts_email.LI_O + ', '.join(simple_list) + cts_email.LI_C


    @classmethod
    def __build_complex_alert(cls, complex_dict):
        """
        Build a complex alert for a given dictionary

        :param complex_dict: the dictionary to build an alert for

        :return: the alert for the complex dictionary
        """
        alert = ''
        for key, val in sorted(complex_dict.items()):
            type_allowed = cts_email.ARROW.join([key, ', '.join(sorted(val))])
            alert += cts_email.LI_O + type_allowed + cts_email.LI_C
        return alert

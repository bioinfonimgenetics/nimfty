# -*- coding: utf-8 -*-

# Gestlab query to get sample type by petition ID
GESTLAB_SAMPLE_TYPE_ID_BY_PETITION_ID = """
    select distinct 
        pp.IDPRUEBA_LISTA as Sample_Type,
        pr2.MAGNITUD as Sample_Desc
    from 
        pet_prueba pp 
    join 
        peticion p 
        on (pp.IDPETICION = p.IDPETICION)
    join
        prueba pr 
        on (pp.IDPRUEBA = pr.IDPRUEBA)
    join 
        prueba pr2 
        on (pp.IDPRUEBA_LISTA = pr2.IDPRUEBA)
    where 
    p.PETICIONCB = '{}' and pp.IDPRUEBA_LISTA in (2402, 2775, 2400, 2872, 3001, 3680)
"""

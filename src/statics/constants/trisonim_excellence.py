# -*- coding: utf-8 -*-

from . import generic as cts

# TrisoNIM Excellence Parameters
GENDER = '2058'
OTHER_ANEUPLO = '2776'
CFFDNA = '2952'
T21 = '2953'
T18 = '2954'
T13 = '2955'
ANEUPLO = '2947'
DEL_DUP = '2948'
METHODOLOGY = '2950'
RESULT = '2951'

CFFDNA_ESTIMATED = 'FRACCIÓN FETAL ESTIMADA: '
HIGH_RISK = 'ALTO RIESGO'
ANEUPLO_LOW = 'Bajo riesgo para la monosomía X. Bajo riesgo para la trisomía XXX. ' \
              'Bajo riesgo para la trisomía XXY. Bajo riesgo para la trisomía XYY.'
ANEUPLO_XO = 'ALTO RIESGO PARA EL SINDROME DE TURNER (X0). Bajo riesgo para la trisomía XXX. ' \
             'Bajo riesgo para la trisomía XXY. Bajo riesgo para la trisomía XYY.'
ANEUPLO_XXY = 'ALTO RIESGO PARA EL SINDROME DE KLINEFELTER (XXY). Bajo riesgo para la monosomía X. ' \
              'Bajo riesgo para la trisomía XXX. Bajo riesgo para la trisomía XYY.'
ANEUPLO_XXX = 'ALTO RIESGO PARA EL SINDROME TRIPLE X (XXX). Bajo riesgo para la monosomía X. ' \
              'Bajo riesgo para la trisomía XXY. Bajo riesgo para la trisomía XYY.'
ANEUPLO_XYY = 'ALTO RIESGO PARA EL SINDROME DE DUPLO Y (XYY). Bajo riesgo para la monosomía X. ' \
              'Bajo riesgo para la trisomía XXX. Bajo riesgo para la trisomía XXY.'

ANEUPLOIDIES_RESULTS_OPTIONS = {
    None: None,
    cts.ANEUPLOIDIES_LOW_RISK: ANEUPLO_LOW,
    cts.XO_HIGH_RISK: ANEUPLO_XO,
    cts.XXX_HIGH_RISK: ANEUPLO_XXX,
    cts.XXY_HIGH_RISK: ANEUPLO_XXY,
    cts.XYY_HIGH_RISK: ANEUPLO_XYY
}

OTHER_ANEUPLOIDIES_CHROMS = ['9', '16', '22']
OTHER_ANEUPLO_LOW = 'Bajo riesgo para trisomía del cromosoma 9. Bajo riesgo para trisomía del cromosoma 16. ' \
                    'Bajo riesgo para trisomía del cromosoma 22.'
OTHER_ANEUPLO_LOW_REST = ' Bajo riesgo para aneuploidías del resto de cromosomas autosómicos.'
OTHER_ANEUPLOIDIES_HIGH = 'ALTO RIESGO PARA TRISOMÍA DEL CROMOSOMA {}. Bajo riesgo para trisomía del' \
                            ' cromosoma {}. Bajo riesgo para trisomía del cromosoma {}.'

DEL_DUP_NOT_DETECTED = 'Bajo riesgo en el estudio de síndromes de microdeleción y microduplicación presentes a' \
                       ' lo largo del genoma con una resolución superior a 5 MB.'
DEL_DUP_DETECTED = 'Se ha realizado un cribado para la determinación del riesgo fetal de presentar algún síndrome' \
                   ' de microdeleción y microduplicación con una resolución superior a 5 MB con el resultado de' \
                   ' ALTO RIESGO para {}'

# TrisoNIM Excellence Methodology
TRISONIM_EXCELLENCE_METHODOLOGY = ''

# TRISONIM_EXCELLENCE_METHODOLOGY = """TrisoNIM® EXCELLENCE es un test de Cribado Prenatal No Invasivo. El estudio se
# realiza con una secuenciación del genoma completo a baja profundidad y se analiza con el algoritmo NIFTY®,
# con marcado CE para el cribado genético no invasivo de la trisomía 21. El algoritmo se basa en un método de conteo en
# el que se comparan las lecturas obtenidas tras la secuenciación con la dosis obtenidas en población sana. Los
# estudios de validación de este cribado indican que TrisoNIM® EXCELLENCE permite estimar la presencia de las trisomías
# 21, 18 y 13 con una especificidad superior al 99,9% y una sensibilidad del 99,17%, 98,24% y 99,9%, respectivamente (
# ver tabla de contigencia al final del informe).
#
# Tras confirmar la presencia de una fracción de ADN circulante fetal superior al 3,5%, y determinar la presencia o
# ausencia de cromosoma Y en la muestra este estudio realiza una estimación del riesgo fetal de presentar las
# siguientes alteraciones cromósomicas:
#
#     - Riesgo de trisomía fetal para los cromosomas 9, 16 y 22, así como la deteccion de aneuploidías en otros
#     cromosomas autosómicos.
#
#     - Riesgo de que el feto pueda ser portador de alteraciones numéricas en los cromosomas sexuales.
#
#     - Riesgo de que el feto padezca los síndromes de microdeleción: 1p36, 1p32-p31, 1q32-q41, 2q33.1, 2p12-p11.2,
#     3pter-p25, 4p16.3, 4q21, 5q12, 5p15 (S. de Cri-du-chat), 5q14.3-q15, 6pter-p24, 6q11-q14, 6q24-q25,
#     8q24.11-q24.13 (S. Langer-Giedion), 9p, 10p14-p13 (S. de DiGeorge 2), 10q26, 11p11.2, 11q23-qter (S. de
#     Jacobsen), 11p13-p12 (S. de WAGR y WAGRO), 14q11-q22, 14q22 (S. de Frías), 15q26-qter (Hernia diafragmática
#     congénita tipo I y síndrome de Drayer), 15q11-q13 (S. de Prader-Willi y S. de Angelman), 16p12-p11, 16q22,
#     17p13.3-11.2 (S. de Miller-Dieker, S. de Smith-Magenis), 18p, 18q y 22q11.2 (S. de DiGeorge)
#
#
#     - Riesgo de que el feto padezca síndromes de microduplicación que afecten a las regiones cromosómicas :
#     15q11-q13, 17p13.3 y 17p12-p11 (S. de Yuan-Harel-Lupski y S. de Potocki-Lupski)
# """

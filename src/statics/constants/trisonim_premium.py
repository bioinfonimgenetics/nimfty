# -*- coding: utf-8 -*-

# TrisoNIM Premium Parameters
CFFDNA = '2054'
T21 = '2055'
T18 = '2056'
T13 = '2057'
GENDER = '2058'
ANEUPLO = '2059'
DEL_DUP = '2332'
OTHER_ANEUPLO = '2333'
METHODOLOGY = '2339'
RESULT = '2344'

# TrisoNIM Premium Methodology
#TRISONIM_PREMIUM_METHODOLOGY = 'Utilizando el ADN fetal extraído a partir de la muestra de sangre materna, ' \
#                               'el cribado prenatal genético no invasivo TrisoNIM® PREMIUM permite estimar el riesgo ' \
#                               'fetal de padecer las trisomías de los cromosomas 21, 18 y 13. El mismo estudio ' \
#                               'permite, de manera informativa, detectar la presencia de ADN del cromosoma Y fetal, ' \
#                               'analizar los cromosomas sexuales, el resto de cromosomas autosómicos y la mayoría de ' \
#                               'las deleciones que producen los síndromes de deleción 1p36, 1q32q41, 2q33.1, ' \
#                               'cri-du-chat (deleción 5p15), 10p14p13, Jacobsen (deleción 11q) y 16p12.2p11.2. El ' \
#                               'estudio se realiza con una secuenciación del genoma completo a baja profundidad y se ' \
#                               'analiza con el algoritmo NIFTY®, con marcado CE para el cribado genético no invasivo ' \
#                               'de la trisomía 21. El algoritmo se basa en un método de conteo en el que se comparan ' \
#                               'las lecturas obtenidas tras la secuenciación con la dosis obtenidas en población ' \
#                               'sana. Este análisis requiere, para su evaluación, una  fracción de ADN circulante ' \
#                               'fetal  igual o superior al 3.5% del total de ADN plasmático analizado. El estadístico' \
#                               ' impide el análisis de una muestra cuando la fracción fetal es inferior a este valor.'

TRISONIM_PREMIUM_METHODOLOGY = ''

# -*- coding: utf-8 -*-

# TrisoNIM Gemelar Parameters
CFFDNA = '2054'
T21 = '2055'
T18 = '2056'
T13 = '2057'
GENDER = '2058'
METHODOLOGY = '2060'
RESULT = '2053'

# TrisoNIM Gemelar Methodology
#TRISONIM_GEMELAR_METHODOLOGY = 'El ADN fetal extraído a partir de la muestra de sangre materna remitida ha sido ' \
#                               'analizado mediante el cribado prenatal genético no invasivo TrisoNIM®con un análisis ' \
#                               'específico para embarazos gemelares, que permite estimar el riesgo fetal de padecer ' \
#                               'las trisomías de cromosomas 21, 18 y 13. El mismo estudio permite, de manera ' \
#                               'informativa detectar la presencia de ADN del cromosoma Y fetal. Esta metodología de ' \
#                               'análisis es derivada, pero diferente, a la empleada para embarazos unitarios. En la ' \
#                               'actualidad no permite establecer un valor numérico exacto de probabilidad de trisomía' \
#                               ' ya que es necesario realizar estudios con poblaciones de mayor tamaño. Es muy ' \
#                               'importante señalar que todas las investigaciones realizadas hasta la fecha indican ' \
#                               'que su sensibilidad y especificidad serían superiores al 99,9% para la detección de ' \
#                               'trisomías de los cromosomas 21, 18 y 13. El estudio se realiza con una secuenciación ' \
#                               'del genoma completo a baja profundidad y se analiza con el algoritmo NIFTY®, ' \
#                               'con marcado CE para el cribado genético no invasivo de la trisomía 21. El algoritmo ' \
#                               'se basa en un método de conteo en el que se comparan las lecturas obtenidas tras la ' \
#                               'secuenciación con la dosis obtenidas en población sana. Este análisis requiere, ' \
#                               'para su evaluación, una  fracción de ADN circulante fetal  igual o superior al 3.5% ' \
#                               'del total de ADN plasmático analizado. El estadístico impide el análisis de una ' \
#                               'muestra cuando la fracción fetal es inferior a este valor.'

TRISONIM_GEMELAR_METHODOLOGY = ''

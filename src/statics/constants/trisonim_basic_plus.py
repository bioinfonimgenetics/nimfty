# -*- coding: utf-8 -*-

# TrisoNIM Basic Plus Parameters
CFFDNA = '2054'
T21 = '2055'
T18 = '2056'
T13 = '2057'
GENDER = '2058'
ANEUPLO = '2059'
METHODOLOGY = '2060'
RESULT = '2053'

# TrisoNIM Basic Plus Methodology
#TRISONIM_BASIC_PLUS_METHODOLOGY = 'Utilizando el ADN fetal extraído a partir de la muestra de sangre materna ' \
#                                  'remitida, el cribado prenatal genético no invasivo TrisoNIM® permite estimar el ' \
#                                  'riesgo fetal de padecer las trisomías de cromosomas 21, 18 y 13. El mismo estudio ' \
#                                  'permite, de manera informativa, detectar la presencia de ADN del cromosoma Y fetal' \
#                                  ' y analizar los cromosomas sexuales. El estudio se realiza con una secuenciación ' \
#                                  'del genoma completo a baja profundidad y se analiza con el algoritmo NIFTY®, ' \
#                                  'con marcado CE para el cribado genético no invasivo de la trisomía 21. El ' \
#                                  'algoritmo se basa en un método de conteo en el que se comparan las lecturas ' \
#                                  'obtenidas tras la secuenciación con la dosis obtenidas en población sana. Este ' \
#                                  'análisis requiere, para su evaluación, una  fracción de ADN circulante fetal  ' \
#                                  'igual o superior al 3.5% del total de ADN plasmático analizado. El estadístico ' \
#                                  'impide el análisis de una muestra cuando la fracción fetal es inferior a este valor.'
TRISONIM_BASIC_PLUS_METHODOLOGY = ''

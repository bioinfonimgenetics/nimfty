# -*- coding: utf-8 -*-

# TrisoNIM
TRISONIM_PREMIUM = 2402
TRISONIM_EXCELLENCE = 2775
TRISONIM_ADVANCE = 2400
TRISONIM_BASIC = 2872
TRISONIM_BASIC_PLUS = 3001
TRISONIM_GEMELAR = 3680
BGI_NIFTY_TESTS_NAMES = {
    2402: 'TrisoNIM Premium',
    2775: 'TrisoNIM Excellence',
    2400: 'TrisoNIM Advance',
    2872: 'TrisoNIM Basic',
    3001: 'TrisoNIM Basic Plus',
    3680: 'TrisoNIM Gemelar'
}

# BGI Columns
PATIENT_NAME = 'Patient_Name'
CFFDNA = 'cffDNA'
T21 = 'T21'
PROB_T21 = 'ProbabilityT21'
T18 = 'T18'
PROB_T18 = 'ProbabilityT18'
T13 = 'T13'
PROB_T13 = 'ProbabilityT13'
T9 = 'T9'
T16 = 'T16'
T22 = 'T22'
XO = 'XO'
XXY = 'XXY'
XXX = 'XXX'
XYY = 'XYY'
SEX_GENO = 'Sex_Genotype_Interpretation'
DEL_DUP = 'Deletion_Duplication'
ANEUPLO = 'IF_Aneuploidy_other_Chr'
DELDUP = 'IF_Chr_del_duplications'
CHR_DEL_DUP = 'IF_Chr_del_duplications'
GENDER = 'Gender'
TIRETYPE = 'TireType'
REPORT_TYPE = 'Report_type'
SAMPLE_ID = 'Sample_ID'
SAMPLE_TYPE = 'Sample_Type'
DF_COLS = [PATIENT_NAME, CFFDNA, T21, PROB_T21, T18, PROB_T18, T13, PROB_T13, T9, T16, T22, XO, XXY, XXX, XYY,
           SEX_GENO, DEL_DUP, ANEUPLO, CHR_DEL_DUP, GENDER, TIRETYPE, REPORT_TYPE]

# BGI Values
NOT_DETECTED = 'Not detected'
NOT_DETECTED_TWINS = 'NOT DETECTED'
DETECTED = 'Detected'
HIGH_RISK = 'High risk'
LOW_RISK = 'Low risk'
OFFICIAL_REPORT = 'official report'
LOW_FETAL = 'low fetal fraction'
ABNORMAL_IND = 'abnormal indicators'
GC_PROBLEM = 'gc problem'
MATERNAL_BG = 'maternal background'
DELAY_REPORT = 'delay report'
RESAMPLING_REPORT = 'resampling report'
REFUND_REPORT = 'refund report'
NONE = 'none'

# General values
FINAL_HIGH_RISK = 'ALTO RIESGO'
MAIN_LOW_RISK = 'LA MUESTRA PRESENTA BAJO RIESGO PARA LAS ANEUPLOIDIAS ANALIZADAS'
MAIN_LOW_RISK_TWINNESS = 'LA MUESTRA GEMELAR PRESENTA BAJO RIESGO PARA LAS ANEUPLOIDIAS ANALIZADAS'
MAIN_HIGH_RISK = 'LA MUESTRA PRESENTA ALTO RIESGO PARA LA ALTERACIÓN GENÉTICA INDICADA. ' \
                 'Se recomienda una técnica genética invasiva para la confirmación del hallazgo.'
MAIN_HIGH_RISK_TWINNESS = 'LA MUESTRA GEMELAR PRESENTA ALTO RIESGO PARA LA ALTERACIÓN GENÉTICA INDICADA. ' \
                          'Se recomienda una técnica genética invasiva para la confirmación del hallazgo.'

# GestLab Petition CB
PETITION_CB = 'PETICIONCB'

# Twiness
TWINS = 'twins'
TRISOMY_LOW_RISK_TWINNESS = 'Trisomía {} Bajo Riesgo'
TRISOMY_HIGH_RISK_TWINNESS = 'TRISOMÍA {} ALTO RIESGO'
GENDER_DETECTED_TWINNESS = 'Embarazo gemelar con detección de cromosoma Y (al menos un feto masculino)'
GENDER_NOT_DETECTED_TWINNESS = 'Embarazo gemelar con ausencia de cromosoma Y (fetos femeninos)'

# cffDNA
LOW_FETAL_VALUE = '< 3.5%'

# Trisomies
TRISOMY_LOW_RISK = 'Trisomía {} Bajo Riesgo             Menor de {}    (riesgo={})'
TRISOMY_HIGH_RISK = 'TRISOMÍA {} ALTO RIESGO    Consultar valor predicitivo positivo en tabla de contingencia'
TRISOMY_CUTOFF_VALUE_HIGHER = '>1/20'
TRISOMY_21 = '21'
TRISOMY_18 = '18'
TRISOMY_13 = '13'

# Gender
GENDER_MALE = 'Masculino (Presencia de cromosoma Y)'
GENDER_FEMALE = 'Femenino (Ausencia de cromosoma Y)'

# Aneuploidies
ANEUPLOIDIES_LOW_RISK = 'Bajo riesgo de aneuploidías sexuales'
ANEUPLOIDIES_NOT_ANALYZED = 'Aneuploidías sexuales no analizadas'
XO_HIGH_RISK = 'Alto riesgo de X0'.upper()
XXX_HIGH_RISK = 'Alto riesgo de XXX'.upper()
XXY_HIGH_RISK = 'Alto riesgo de XXY'.upper()
XYY_HIGH_RISK = 'Alto riesgo de XYY'.upper()
ANEUPLOIDIES_HIGH_RISKS = [XO_HIGH_RISK, XXX_HIGH_RISK, XXY_HIGH_RISK, XYY_HIGH_RISK]

# Deletions/duplications
DEL_DUP_LOW_RISK = 'Bajo riesgo para las microdeleciones analizadas'
DEL_DUP_HIGH_RISK = 'Alto riesgo para la deleción {}'

# Other aneuploidies
OTHER_ANEUPLOIDIES_LOW_RISK = 'Bajo riesgo de aneuploidías del resto de cromosomas autosómicos'
OTHER_ANEUPLOIDIES_HIGH_RISKS = ['Alto riesgo de trisomía en el cromosoma 9'.upper(),
                                 'Alto riesgo de trisomía en el cromosoma 16'.upper(),
                                 'Alto riesgo de trisomía en el cromosoma 22'.upper()]

# Delay
DELAY = 'RETRASO DE EMISIÓN DE INFORME POR REPETICIÓN DE LIBRERÍA. Los datos de calidad del análisis se encuentran ' \
        'fuera de los parámetros óptimos recomendados. Este resultado inhabilita cualquier análisis posterior, ' \
        'impidiendo la estimación de riesgo de cualquiera de las tres trisomías. Para obtener un resultado, ' \
        'se está realizando una nueva librería genómica a partir del plasma de la sangre materna, lo que supone un ' \
        'tiempo estimado de retraso de 5 días laborables.'

# Resampling
RESAMPLING = 'PETICIÓN DE NUEVA MUESTRA PARA ANÁLISIS. El análisis indica una proporción' \
             ' insuficiente de ADN fetal, al ser comparado con la proporción de ADN materno ' \
             'en plasma. Este resultado inhabilita cualquier análisis posterior, impidiendo' \
             ' la estimación de riesgo de cualquiera de las tres trisomías. Se precisa una' \
             ' nueva muestra de sangre, tomada en un tiempo diferente al de la muestra original,' \
             ' para completar el análisis.'
# Refund
REFUND = 'RESULTADO NO ANALIZABLE. ANÁLISIS NO CONCLUYENTE. El análisis indica' \
          ' una proporción insuficiente de ADN fetal, al ser comparado con la ' \
          'proporción de ADN materno en plasma. Este resultado inhabilita ' \
          'cualquier análisis, impidiendo la estimación de riesgo de cualquiera' \
          ' de las tres trisomías. Se recomienda que la paciente siga bajo ' \
          'supervisión médica según los procedimientos habituales.'

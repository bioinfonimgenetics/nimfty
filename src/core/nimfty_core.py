# -*- coding: utf-8 -*-

from handlers.nimfty_handler import NimftyHandler
from handlers.alerts_handler import AlertsHandler


# noinspection SpellCheckingInspection
class NimftyCore:
    """
    Instantiates all the different modules and handles all the logic of the execution
    """
    def __init__(self): pass


    @staticmethod
    def run_nimfty():
        """
        Executes NIMFty process

        :return: None
        """
        # NIMFty handler
        nimfty_handler = NimftyHandler()
        # Read BGI NIFty files
        nimfty_handler.read_bgi_files()
        # If files exist to process...
        if nimfty_handler.bgi_files():
            # ...format BGI info
            nimfty_handler.format_bgi_info()
            # Prepare GestLab structures
            nimfty_handler.prepare_gestlab_structures()
            # Remove files from BGI folder
            nimfty_handler.clear_bgi_folder()
            # Send alerts
            AlertsHandler.send_alerts()

        return

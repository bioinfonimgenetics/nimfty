# -*- coding: utf-8 -*-

import os
import logging

import pandas as pd

from statics.constants import generic as cts
from config.config_parser import ConfigParser
from structs.trisonim_excellence import TrisonimExcellence
from structs.trisonim_basic_plus import TrisonimBasicPlus
from structs.trisonim_premium import TrisonimPremium
from structs.trisonim_advance import TrisonimAdvance
from structs.trisonim_gemelar import TrisonimGemelar
from structs.trisonim_basic import TrisonimBasic


class StructuresController:
    """
    Defines all the operations that are required to process the different types of TrisoNIM Stuctures
    """
    def __init__(self):
        """ Constructor """
        self._trisonim_structures_logger = logging.getLogger('trisonim_structures')
        # TrisoNIM types
        self.trisonim_types = {
            cts.TRISONIM_PREMIUM:       TrisonimPremium,
            cts.TRISONIM_EXCELLENCE:    TrisonimExcellence,
            cts.TRISONIM_ADVANCE:       TrisonimAdvance,
            cts.TRISONIM_BASIC:         TrisonimBasic,
            cts.TRISONIM_BASIC_PLUS:    TrisonimBasicPlus,
            cts.TRISONIM_GEMELAR:       TrisonimGemelar
        }


    def dispatch_group_by_sample_type(self, group_info, sample_type):
        """
        Processes each sample in the group according to the type of TrisoNIM product that is

        :param group_info: samples grouped by type (dataframe)
        :param sample_type: the type of TrisoNIM product

        :return: a dataframe with the processed samples grouped by type
        """
        self._trisonim_structures_logger.info(f'Dispatching group for sample type {sample_type}')
        trisonim_samples = []
        for row in group_info.itertuples():
            # Get the proper TrisoNIM type
            trisonim_type = self.trisonim_types[sample_type]()
            # Process the TrisoNIM group of samples
            trisonim_samples.append(trisonim_type.process_trisonim_sample(row))
        return pd.DataFrame(trisonim_samples)


    def write_group_by_sample_type(self, group_results, sample_type):
        """
        Writes the information of the processed samples grouped by type into a file

        :param group_results: a dataframe with the processed samples grouped by type
        :param sample_type: the type of TrisoNIM product

        :return: None
        """
        self._trisonim_structures_logger.info(f'Writing group for sample type {sample_type}')
        path = os.path.join(ConfigParser.get_conf("PATHS", "structures_folder"), str(int(sample_type)) + '.csv')
        group_results.to_csv(path, mode='a', sep='|', index=None, encoding='ISO-8859-1')

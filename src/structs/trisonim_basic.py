# -*- coding: utf-8 -*-

from structs.trisonim_super import TrisonimSuper
from statics.constants import (generic as cts, trisonim_basic as cts_tb)


class TrisonimBasic(TrisonimSuper):
    """
    Defines all the operations to compute the different results for TrisoNIM Basic structure
    Inherited from TrisoNIM Base class
    """
    def __init__(self):
        """ Constructor """
        # Super init
        TrisonimSuper.__init__(self)


    def fill_structure_values(self):
        """
        Retrieves the TrisoNIM Advance results by filling a dictionary (id_result, result_value)
        Override from base class TrisoNIM Base

        :return: the results
        """
        self.structure[cts.PETITION_CB] = self.petitioncb
        self.structure[cts_tb.CFFDNA] = self.cffdna
        self.structure[cts_tb.T21] = self.trisomies[cts.TRISOMY_21]
        self.structure[cts_tb.T18] = self.trisomies[cts.TRISOMY_18]
        self.structure[cts_tb.T13] = self.trisomies[cts.TRISOMY_13]
        self.structure[cts_tb.METHODOLOGY] = cts_tb.TRISONIM_BASIC_METHODOLOGY
        self.structure[cts_tb.RESULT] = self._check_final_risk()




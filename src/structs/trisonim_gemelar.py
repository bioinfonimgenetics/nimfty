# -*- coding: utf-8 -*-

from structs.trisonim_super import TrisonimSuper
from statics.constants import (generic as cts, trisonim_gemelar as cts_tg)


class TrisonimGemelar(TrisonimSuper):
    """
    Defines all the operations to compute the different results for TrisoNIM Gemelar structure
    Inherited from TrisoNIM Base class
    """
    def __init__(self):
        """ Constructor """
        # Super init
        TrisonimSuper.__init__(self)


    def fill_structure_values(self):
        """
        Retrieves the TrisoNIM Premium results by filling a dictionary (id_result, result_value)
        Override from base class TrisoNIM Base

        :return: the results
        """
        self.structure[cts.PETITION_CB] = self.petitioncb
        self.structure[cts_tg.CFFDNA] = self.cffdna
        self.structure[cts_tg.T21] = self.trisomies[cts.TRISOMY_21]
        self.structure[cts_tg.T18] = self.trisomies[cts.TRISOMY_18]
        self.structure[cts_tg.T13] = self.trisomies[cts.TRISOMY_13]
        self.structure[cts_tg.GENDER] = self.gender
        self.structure[cts_tg.METHODOLOGY] = cts_tg.TRISONIM_GEMELAR_METHODOLOGY
        self.structure[cts_tg.RESULT] = self._check_final_risk()




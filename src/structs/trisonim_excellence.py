# -*- coding: utf-8 -*-

from structs.trisonim_super import TrisonimSuper
from statics.constants import (generic as cts, trisonim_excellence as cts_te)


class TrisonimExcellence(TrisonimSuper):
    """
    Defines all the operations to compute the different results for TrisoNIM Excellence structure
    Inherited from TrisoNIM Base class
    """
    def __init__(self):
        """ Constructor """
        # Super init
        TrisonimSuper.__init__(self)


    def fill_structure_values(self):
        """
        Retrieves the TrisoNIM Excellence results by filling a dictionary (id_result, result_value)
        Override from base class TrisoNIM Base

        :return: the results
        """
        self.structure[cts.PETITION_CB] = self.petitioncb
        self.structure[cts_te.CFFDNA] = cts_te.CFFDNA_ESTIMATED + self.cffdna if self.cffdna else None
        self.structure[cts_te.T21] = self.trisomies[cts.TRISOMY_21]
        self.structure[cts_te.T18] = self.trisomies[cts.TRISOMY_18]
        self.structure[cts_te.T13] = self.trisomies[cts.TRISOMY_13]
        self.structure[cts_te.GENDER] = self.gender
        self.structure[cts_te.ANEUPLO] = cts_te.ANEUPLOIDIES_RESULTS_OPTIONS[self.aneuploidies]
        self.structure[cts_te.DEL_DUP] = self._retrieve_del_dups()
        self.structure[cts_te.OTHER_ANEUPLO] = self._retrieve_other_aneuploidies_result()
        self.structure[cts_te.METHODOLOGY] = cts_te.TRISONIM_EXCELLENCE_METHODOLOGY
        self.structure[cts_te.RESULT] = self._check_final_risk()


    def _retrieve_del_dups(self):
        """
        Retrieves the final result for deletions/duplications according to the risk value

        :return: None
        """
        del_dups = None
        if self.del_dup:
            if self.del_dup == cts.DEL_DUP_LOW_RISK:
                del_dups = cts_te.DEL_DUP_NOT_DETECTED
            else:
                info_chr = self.del_dup.split()[-1]
                del_dups = cts_te.DEL_DUP_DETECTED.format(info_chr)
        return del_dups


    def _retrieve_other_aneuploidies_result(self):
        """
        Retrieves the final result for other aneuploidies according to the risk value

        :return: None
        """
        other_aneuploidies = None
        if self.other_aneuploidies:
            if self.other_aneuploidies == cts.OTHER_ANEUPLOIDIES_LOW_RISK:
                other_aneuploidies = cts_te.OTHER_ANEUPLO_LOW
            else:
                trisomy_chrom = self.other_aneuploidies.split()[-1]
                other_aneuploidies_chroms = cts_te.OTHER_ANEUPLOIDIES_CHROMS
                other_aneuploidies_chroms.remove(trisomy_chrom)
                other_aneuploidies_chroms.insert(0, trisomy_chrom)
                other_aneuploidies = cts_te.OTHER_ANEUPLOIDIES_HIGH.format(*other_aneuploidies_chroms)

            if cts.NOT_DETECTED in self.info_other_chr:
                other_aneuploidies += cts_te.OTHER_ANEUPLO_LOW_REST

        return other_aneuploidies


# -*- coding: utf-8 -*-

from structs.trisonim_super import TrisonimSuper
from statics.constants import (generic as cts, trisonim_premium as cts_tp)


class TrisonimPremium(TrisonimSuper):
    """
    Defines all the operations to compute the different results for TrisoNIM Premium structure
    Inherited from TrisoNIM Base class
    """
    def __init__(self):
        """ Constructor """
        # Super init
        TrisonimSuper.__init__(self)


    def fill_structure_values(self):
        """
        Retrieves the TrisoNIM Premium results by filling a dictionary (id_result, result_value)
        Override from base class TrisoNIM Base

        :return: the results
        """
        self.structure[cts.PETITION_CB] = self.petitioncb
        self.structure[cts_tp.CFFDNA] = self.cffdna
        self.structure[cts_tp.T21] = self.trisomies[cts.TRISOMY_21]
        self.structure[cts_tp.T18] = self.trisomies[cts.TRISOMY_18]
        self.structure[cts_tp.T13] = self.trisomies[cts.TRISOMY_13]
        self.structure[cts_tp.GENDER] = self.gender
        self.structure[cts_tp.ANEUPLO] = self.aneuploidies
        self.structure[cts_tp.DEL_DUP] = self.del_dup
        self.structure[cts_tp.OTHER_ANEUPLO] = self.other_aneuploidies
        self.structure[cts_tp.METHODOLOGY] = cts_tp.TRISONIM_PREMIUM_METHODOLOGY
        self.structure[cts_tp.RESULT] = self._check_final_risk()




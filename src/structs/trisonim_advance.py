# -*- coding: utf-8 -*-

from structs.trisonim_super import TrisonimSuper
from statics.constants import (generic as cts, trisonim_advance as cts_ta)


class TrisonimAdvance(TrisonimSuper):
    """
    Defines all the operations to compute the different results for TrisoNIM Advance structure
    Inherited from TrisoNIM Base class
    """
    def __init__(self):
        """ Constructor """
        # Super init
        TrisonimSuper.__init__(self)


    def fill_structure_values(self):
        """
        Retrieves the TrisoNIM Advance results by filling a dictionary (id_result, result_value)
        Override from base class TrisoNIM Base

        :return: the results
        """
        self.structure[cts.PETITION_CB] = self.petitioncb
        self.structure[cts_ta.CFFDNA] = self.cffdna
        self.structure[cts_ta.T21] = self.trisomies[cts.TRISOMY_21]
        self.structure[cts_ta.T18] = self.trisomies[cts.TRISOMY_18]
        self.structure[cts_ta.T13] = self.trisomies[cts.TRISOMY_13]
        self.structure[cts_ta.GENDER] = self.gender
        self.structure[cts_ta.ANEUPLO] = self.aneuploidies
        self.structure[cts_ta.DEL_DUP] = self.del_dup
        self.structure[cts_ta.OTHER_ANEUPLO] = self.other_aneuploidies
        self.structure[cts_ta.METHODOLOGY] = cts_ta.TRISONIM_ADVANCE_METHODOLOGY
        self.structure[cts_ta.RESULT] = self._check_final_risk()




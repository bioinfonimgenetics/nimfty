# -*- coding: utf-8 -*-

import logging
from fractions import Fraction

import pandas as pd

import errors.errors as e
from handlers.alerts_handler import AlertsHandler
from statics.constants import generic as cts


class TrisonimSuper(object):
    """
    Defines all the common operations to compute the different results for TrisoNIM Products
    """
    def __init__(self):
        """ Constructor """
        self._trisonim_logger = logging.getLogger('trisonim_base')

        # TrisoNIM Structure
        self.structure = None

        # TrisoNIM Parameters
        self._twinness = False
        self.petitioncb = None
        self.cffdna = None
        self.trisomies = {cts.TRISOMY_21: None, cts.TRISOMY_18: None, cts.TRISOMY_13: None}
        self.gender = None
        self.aneuploidies = None
        self.del_dup = None
        self.other_aneuploidies = None
        self.info_other_chr = None
        self.request = None


    def process_trisonim_sample(self, sample_info):
        """
        Process TrisoNIM values for a given sample information

        :param sample_info: the sample info entry

        :return: None
        """
        self.structure = {}

        self._setpetitioncb(sample_info)       # Petition CB
        self._set_twinness(sample_info)         # Twinness property

        # Retrieve report type field, the most important of all because gives a lot of info about the result
        report_type = sample_info.Report_type.lower()

        if report_type != cts.OFFICIAL_REPORT:
            self._analyze_report_types(report_type)
        else:
            # Compute common fields
            self._set_cffdna_value(sample_info)             # cffDNA
            self._analyze_trisomies(sample_info)            # Trisomies 21, 18 & 13
            self._analyze_gender(sample_info)               # Gender
            self._analyze_aneuploidies(sample_info)         # Sexual aneuploidies
            self._analyze_dels_dups(sample_info)            # Microdeletions/duplications
            self._analyze_other_aneuploidies(sample_info)   # Microdeletions/duplications

        self._trisonim_logger.info('Retrieving final results...')

        self.fill_structure_values()

        return self.structure


    def fill_structure_values(self):
        """
        Retrieves the TrisoNIM results and fills the proper structure with them

        :return: the results
        """
        pass


    def _setpetitioncb(self, sample_info):
        """
        Sets the petitioncb GestLab identifier

        :param sample_info: the sample info entry

        :return: None
        """
        petition = sample_info.Sample_ID
        self.petitioncb = petition
        self._trisonim_logger.info(f'Processing Gestlab info for {petition}')


    def _set_twinness(self, sample_info):
        """
        Sets the twinness property (true or false)

        :param sample_info: the sample info entry

        :return: None
        """
        self._twinness = sample_info.TireType == cts.TWINS
        self._trisonim_logger.info(f'Twinnes: {self._twinness}')


    def _set_cffdna_value(self, sample_info):
        """
        Sets the cffDNA value

        :param sample_info: the sample info entry

        :return: None
        """
        cffdna = sample_info.cffDNA if not pd.isnull(sample_info.cffDNA) else e.raise_nimfty_error(cts.CFFDNA)
        self.cffdna = str(cffdna) + '%'
        self._trisonim_logger.info(f'cffDNA: {self.cffdna}')


    def _analyze_report_types(self, report_type):
        """
        Analyzes the different types of reports for a given sample info, this is, delay, resampling or refund

        :param report_type: the type of the report (delay, resampling or refund)

        :return: None
        """
        report_types_functions = {
            cts.DELAY_REPORT: lambda: self._analyze_delay(),
            cts.RESAMPLING_REPORT: lambda: self._analyze_resampling(),
            cts.REFUND_REPORT: lambda: self._analyze_refund()
        }
        report_types_functions[report_type]()


    def _analyze_delay(self):
        """
        Given a sample info, analyzes if it is delayed (relibrary)

        :return: None
        """
        self.request = cts.DELAY
        self.cffdna = None
        AlertsHandler.alerts_samples_requests[self.petitioncb].append(cts.DELAY_REPORT.capitalize())
        self._trisonim_logger.info(f'Sample requested: {cts.DELAY_REPORT.capitalize()}')


    def _analyze_resampling(self):
        """
        Given a sample info, analyzes if it is resampling
        :return: None
        """
        self.request = cts.RESAMPLING
        self.cffdna = cts.LOW_FETAL_VALUE
        AlertsHandler.alerts_samples_requests[self.petitioncb].append(cts.RESAMPLING_REPORT.capitalize())
        self._trisonim_logger.info(f'Sample requested: {cts.RESAMPLING_REPORT.capitalize()}')


    def _analyze_refund(self):
        """
        Given a sample info, analyzes if it is refund

        :return: None
        """
        self.request = cts.REFUND
        self.cffdna = cts.LOW_FETAL_VALUE
        AlertsHandler.alerts_samples_requests[self.petitioncb].append(cts.REFUND_REPORT.capitalize())
        self._trisonim_logger.info(f'Sample requested: {cts.REFUND_REPORT.capitalize()}')


    def _analyze_trisomies(self, sample_info):
        """
        Given a sample info, analyzes its trisomies risk value based on the rest of the info

        :param sample_info: the info

        :return: None
        """
        if self._twinness:
            self._compute_trisomy_risk_twinness(sample_info.T21, cts.TRISOMY_21)
            self._compute_trisomy_risk_twinness(sample_info.T18, cts.TRISOMY_18)
            self._compute_trisomy_risk_twinness(sample_info.T13, cts.TRISOMY_13)
        else:
            self._compute_trisomy_risk(sample_info.ProbabilityT21, cts.TRISOMY_21)
            self._compute_trisomy_risk(sample_info.ProbabilityT18, cts.TRISOMY_18)
            self._compute_trisomy_risk(sample_info.ProbabilityT13, cts.TRISOMY_13)


    def _compute_trisomy_risk(self, trisomy_prob_value, chrom):
        """
        Computes the risk of a given trisomy probability

        :param trisomy_prob_value: the trisomy probability
        :param chrom: the chromosome of the trisomy

        :return: None
        """
        # Get risk value
        trisomy_risk = ''
        if trisomy_prob_value == cts.TRISOMY_CUTOFF_VALUE_HIGHER:
            # High risk
            trisomy_msg = cts.TRISOMY_HIGH_RISK if not self._twinness else cts.TRISOMY_HIGH_RISK_TWINNESS
            trisomy_risk = trisomy_msg.format(chrom)
            AlertsHandler.alerts_samples_high_risk[self.petitioncb].append(f'Trisomy {chrom}')
        else:
            numerator, denominator = trisomy_prob_value.split('/')
            risk_value = Fraction(numerator) / Fraction(denominator)
            # Check low risk risk_value
            cutoffs, cutoffs_values = self._check_low_risk_range(risk_value)
            if sum(cutoffs) >= 1:
                for cutoff, cutoff_value in zip(cutoffs, cutoffs_values):
                    if cutoff:
                        trisomy_msg = cts.TRISOMY_LOW_RISK if not self._twinness else cts.TRISOMY_LOW_RISK_TWINNESS
                        trisomy_risk = trisomy_msg.format(chrom, cutoff_value, risk_value)
                        break

        self.trisomies[chrom] = trisomy_risk
        self._trisonim_logger.info(f'Trisomy {chrom}: {trisomy_risk}')


    def _compute_trisomy_risk_twinness(self, trisomy_value, chrom):
        """
        Computes the risk of a given trisomy value

        :param trisomy_value: the trisomy value
        :param chrom: the chromosome of the trisomy

        :return: None
        """
        # Get risk value
        trisomy_risk = ''
        if trisomy_value == cts.LOW_RISK:
            trisomy_risk = cts.TRISOMY_LOW_RISK_TWINNESS.format(chrom)
        elif trisomy_value == cts.HIGH_RISK:
            trisomy_risk = cts.TRISOMY_HIGH_RISK_TWINNESS.format(chrom)
            AlertsHandler.alerts_samples_high_risk[self.petitioncb].append(f'Trisomy {chrom}')

        self.trisomies[chrom] = trisomy_risk
        self._trisonim_logger.info(f'Trisomy {chrom}: {trisomy_risk}')


    def _analyze_gender(self, sample_info):
        """
        Given a sample info, analyze its gender/fetal sex

        :param sample_info: the info

        :return: None
        """
        g = sample_info.Gender if not pd.isnull(sample_info.Gender) else e.raise_nimfty_error(cts.GENDER)
        if self._twinness:
            if g.upper() == cts.NOT_DETECTED_TWINS:
                self.gender = cts.GENDER_NOT_DETECTED_TWINNESS
            elif g == cts.DETECTED:
                self.gender = cts.GENDER_DETECTED_TWINNESS
        else:
            self.gender = cts.GENDER_MALE if g == 'Male' else cts.GENDER_FEMALE
        self._trisonim_logger.info(f'Gender: {self.gender}')


    def _analyze_aneuploidies(self, sample_info):
        """
        Given a sample info, analyze its sexual aneuploidies

        :param sample_info: the info

        :return: None
        """
        if not self._twinness:
            xo = sample_info.XO if not pd.isnull(sample_info.XO) else e.raise_nimfty_error(cts.XO)
            xxy = sample_info.XXY if not pd.isnull(sample_info.XXY) else e.raise_nimfty_error(cts.XXY)
            xxx = sample_info.XXX if not pd.isnull(sample_info.XXX) else e.raise_nimfty_error(cts.XXX)
            xyy = sample_info.XYY if not pd.isnull(sample_info.XYY) else e.raise_nimfty_error(cts.XYY)

            self.aneuploidies = cts.ANEUPLOIDIES_LOW_RISK

            for aneuploidie, risk in zip([xo, xxx, xxy, xyy], cts.ANEUPLOIDIES_HIGH_RISKS):
                if aneuploidie != cts.NOT_DETECTED:
                    self.aneuploidies = risk
                    AlertsHandler.alerts_samples_high_risk[self.petitioncb].append(f'Sexual aneuploidies')
                    break

            self._trisonim_logger.info(f'Sexual aneuploidies: {self.aneuploidies}')


    def _analyze_dels_dups(self, sample_info):
        """
        Given a sample info, analyze its deletions & duplicationsw

        :param sample_info: the info

        :return: None
        """
        if not self._twinness:
            dd = sample_info.Deletion_Duplication if not pd.isnull(
                sample_info.Deletion_Duplication) else e.raise_nimfty_error(cts.DEL_DUP)
            if dd == cts.NOT_DETECTED:
                self.del_dup = cts.DEL_DUP_LOW_RISK
            else:
                chrom = sample_info.IF_Chr_del_duplications if not pd.isnull(
                    sample_info.IF_Chr_del_duplications) else e.raise_nimfty_error(cts.CHR_DEL_DUP)
                self.del_dup = cts.DEL_DUP_HIGH_RISK.format(chrom)
                AlertsHandler.alerts_samples_high_risk[self.petitioncb].append(f'Deletion/duplication')

            # Check other relevant info
            self.chr_del_dups = sample_info.IF_Chr_del_duplications if not pd.isnull(
                sample_info.IF_Chr_del_duplications) else e.raise_nimfty_error(cts.DELDUP)
            if cts.NOT_DETECTED not in self.chr_del_dups and not self.chr_del_dups.lower().endswith(cts.NONE):
                AlertsHandler.alerts_samples_info[self.petitioncb].append(self.chr_del_dups)

            self._trisonim_logger.info(f'Microdeletions/duplications: {self.del_dup}')


    def _analyze_other_aneuploidies(self, sample_info):
        """
        Given a sample info, analyze another sexual aneuploidies

        :param sample_info: the info

        :return: None
        """
        if not self._twinness:
            t9 = sample_info.T9 if not pd.isnull(sample_info.T9) else e.raise_nimfty_error(cts.T9)
            t16 = sample_info.T16 if not pd.isnull(sample_info.T16) else e.raise_nimfty_error(cts.T16)
            t22 = sample_info.T22 if not pd.isnull(sample_info.T22) else e.raise_nimfty_error(cts.T22)

            self.other_aneuploidies = cts.OTHER_ANEUPLOIDIES_LOW_RISK

            for other_aneuploidie, risk in zip([t9, t16, t22], cts.OTHER_ANEUPLOIDIES_HIGH_RISKS):
                if other_aneuploidie == cts.HIGH_RISK:
                    self.other_aneuploidies = risk
                    AlertsHandler.alerts_samples_high_risk[self.petitioncb].append(f'Other aneuploidies')
                    break

            # Check other relevant info
            self.info_other_chr = sample_info.IF_Aneuploidy_other_Chr if not pd.isnull(
                sample_info.IF_Aneuploidy_other_Chr) else e.raise_nimfty_error(cts.ANEUPLO)
            if cts.NOT_DETECTED not in self.info_other_chr:
                AlertsHandler.alerts_samples_info[self.petitioncb].append(self.info_other_chr)

            self._trisonim_logger.info(f'Other sexual aneuploidies: {self.other_aneuploidies}')


    @staticmethod
    def _check_low_risk_range(trisomie_prob_value):
        """
        Checks the value ranges for a low-risked trisomy

        :param trisomie_prob_value: the value to check (trisomy)

        :return: the list with the evaluations
        """
        return [trisomie_prob_value <= Fraction('1/100000'),
                Fraction('1/50000') >= trisomie_prob_value > Fraction('1/100000'),
                Fraction('1/10000') >= trisomie_prob_value > Fraction('1/50000'),
                Fraction('1/5000') >= trisomie_prob_value > Fraction('1/10000'),
                Fraction('1/1000') >= trisomie_prob_value > Fraction('1/5000'),
                Fraction('1/500') >= trisomie_prob_value > Fraction('1/1000'),
                Fraction('1/20') > trisomie_prob_value > Fraction('1/500')],\
               ['0.001%', '0.002%', '0.01%', '0.02%', '0.1%', '0.2%', '1%']


    def _check_final_risk(self):
        """
        Checks the final risk result for the rest of TrisoNIM values

        :return: the final risk
        """
        final_risk = cts.MAIN_LOW_RISK if not self._twinness else cts.MAIN_LOW_RISK_TWINNESS

        if self.request:
            final_risk = self.request
        else:
            for value in self.structure.values():
                if value and cts.FINAL_HIGH_RISK in value.upper():
                    final_risk = cts.MAIN_HIGH_RISK if not self._twinness else cts.MAIN_HIGH_RISK_TWINNESS
                    break

        return final_risk


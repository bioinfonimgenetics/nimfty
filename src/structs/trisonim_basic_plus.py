# -*- coding: utf-8 -*-

from structs.trisonim_super import TrisonimSuper
from statics.constants import (generic as cts, trisonim_basic_plus as cts_tbp)


class TrisonimBasicPlus(TrisonimSuper):
    """
    Defines all the operations to compute the different results for TrisoNIM Basic Plus structure
    Inherited from TrisoNIM Base class
    """
    def __init__(self):
        """ Constructor """
        # Super init
        TrisonimSuper.__init__(self)


    def fill_structure_values(self):
        """
        Retrieves the TrisoNIM Advance results by filling a dictionary (id_result, result_value)
        Override from base class TrisoNIM Base

        :return: the results
        """
        self.structure[cts.PETITION_CB] = self.petitioncb
        self.structure[cts_tbp.CFFDNA] = self.cffdna
        self.structure[cts_tbp.T21] = self.trisomies[cts.TRISOMY_21]
        self.structure[cts_tbp.T18] = self.trisomies[cts.TRISOMY_18]
        self.structure[cts_tbp.T13] = self.trisomies[cts.TRISOMY_13]
        self.structure[cts_tbp.GENDER] = self.gender
        self.structure[cts_tbp.ANEUPLO] = self.aneuploidies
        self.structure[cts_tbp.METHODOLOGY] = cts_tbp.TRISONIM_BASIC_PLUS_METHODOLOGY
        self.structure[cts_tbp.RESULT] = self._check_final_risk()




# -*- coding: utf-8 -*-

import sys
from nimutils import dashboard as nim_dashboard


def raise_nimfty_error(msg):
    """
    Shows the raised error message and interrupts the execution

    :param msg: error message

    :return: None
    """
    nim_dashboard.program_status(msg, type_msg='error')
    sys.exit(1)


def raise_nimfty_warning(msg):
    """
    Shows the raised warning message

    :param msg: warning message

    :return: None
    """
    nim_dashboard.program_status(msg, type_msg='warning')



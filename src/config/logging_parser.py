# -*- coding: utf-8 -*-

import logging.config
from os import path, makedirs
from config.config_parser import ConfigParser


# noinspection SpellCheckingInspection
class LoggingParser:
    """
    Module to initialize the logging system that reads all the different logging module in logging.cfg
    """
    def __init__(self): pass

    @classmethod
    def initialize_logging(cls, id_execution):
        """
        Initialize logging handler

        :param id_execution: the execution ID

        :return: None
        """
        _parent_dir = path.dirname(path.dirname(path.dirname(path.abspath(__file__))))
        _log_folder = path.join(ConfigParser.get_conf('LOGS', 'path_logs'))
        _conf_folder = path.join(_parent_dir, 'cfg')

        if not path.exists(_log_folder):
            makedirs(_log_folder)
        logname = f'nimfty_{id_execution}.log'
        logfilename = path.join(_log_folder, logname)
        logging.config.fileConfig(path.join(path.realpath(_conf_folder), 'logging.conf'),
                                  defaults={'logfilename': logfilename})

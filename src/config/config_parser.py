# -*- coding: utf-8 -*-

from configparser import ConfigParser
from os import path


# noinspection PyRedeclaration
class ConfigParser:
    """
    Module to initialize the ConfigParser that reads all the entries defined in cfg.ini
    """
    config_parser = ConfigParser(allow_no_value=True)

    def __init__(self): pass

    @classmethod
    def initialize_config(cls):
        """
        Initialize config handler

        :return: None
        """
        _parent_dir = path.dirname(path.dirname(path.dirname(path.abspath(__file__))))
        _config_folder = path.join(_parent_dir, 'cfg')
        _config_file_path = path.join(path.realpath(_config_folder), 'cfg.ini')
        cls.config_parser.read(_config_file_path)


    @classmethod
    def get_conf(cls, section, key):
        """
        Retrieves value for a key in a section

        :param section: section name
        :param key: key name

        :return: None if not found
        """
        return cls.config_parser.get(section, key)

[TOC]


# **NIMFty** #

![nimfty.png](https://bitbucket.org/repo/zXz5aLx/images/2421347244-nimfty.png)

## **Overview**

This project NIMFty automates a process for the massive insertion of data from BGI NIFTY to Gestlab.

The different types of products it handles are:

+ TrisoNIM Premium
+ TrisoNIM Excellence
+ TrisoNIM Advance
+ TrisoNIM Gemelar
+ TrisoNIM Basic Plus
+ TrisoNIM Basic

## **Configuration & Installation**

**STEP 1**

First of all, it is necessary to update the current version of _pip_ and some libraries in _PGM_:

```
1) sudo python -m pip install --upgrade pip
2) sudo python -m pip install --upgrade pandas
3) sudo python -m pip install xlrd
```

If you see 'Successfully installed!' or something similar, please continue. If not, please ask for help.

In _NIMFty_, a direct connection between the local machine and _Gestlab DB_ is established in order to execute some queries to extract information about the petitions, so please install (_PGM_ is _UNIX_ server):

```
1) sudo apt install libfbclient2
2) sudo python -m pip install fdb
```

**STEP 2**

If _NIMFty_ module is already installed, please go to the next section 'Execution'.

Next step will be necessary to download repository from Git (use _sudo_ if necessary):

```
1) cd /opt/repos 
1) sudo git clone https://solarpab@bitbucket.org/bioinfonimgenetics/nimfty.git
```

**STEP 3**

The repository will be created and you could navigate to the module itself:

```
cd nimfty
```

There you could see the structure of folders, files, etc. if you are interested.

## **Execution**

_NIMFty_ requires no parameteres, so its simply executed every hour by an automated process each with the following instruction:

```
python src/nimfty.py
```

## **Final Email**

_NIMFty_ sends a friendly automatic email to some recipients to inform about the results. It is composed of two sections:

**NIMFty Information**

+ **_Files read_**  ->  the files that will be read from the cloud in every execution

+ **_Samples included_**  ->  samples that are processed for being TrisoNIM Premium or Excellence

+ **_Samples excluded_**  ->  samples that are not processed for not being TrisoNIM Premium or Excellence

**NIMFty Results**

+ **_Samples with high risk_**  ->  samples with high risk (trisomies, aneuplodies, microdeletion syndromes, etc.)

+ **_Samples with requests_**  ->  samples that need resampling, refund or relibrary

+ **_Samples with relevant information_**  ->  samples that have any kind of information in _IF_Aneuploidy_other_Chr_, a very important BGI field that needs to be checked
```